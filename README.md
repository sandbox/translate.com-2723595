# Translate.com Website Translator

## What is this module?
The purpose of this module is to provide a much easier implementation of the Translate.com Website 
Translator onto a Drupal site. Rather than having to copy and paste javascript into the pages 
themselves, this application will auto generate and insert the Javascript block for you, based off 
options you set in the settings menu.

## More info
You can find more info [here][https://www.translate.com/blog/].
